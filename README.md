# Discord Reputation Bot

A Discord bot that tracks a user's reputation within and across servers. A user can gain or lose reputation based on their actions, as well as gain points for positive actions.